﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm.aspx.cs" Inherits="website.WebForm" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <link href="Style.css"/ type="text/css" rel="stylesheet">
</head>
    <body>
	<div class="wrapper">
		<div class="line line1">
			<span class="circle circle-top"></span>
			<div class="dotted">
				<span class="dot dot-top"></span>
				<span class="dot dot-middle-top"></span>
				<span class="dot dot-middle-bottom"></span>
				<span class="dot dot-bottom"></span>
			</div>
			<span class="circle circle-bottom"></span>
		</div>
		<div class="line line2">
			<span class="circle circle-top"></span>
			<div class="dotted">
				<span class="dot dot-top"></span>
				<span class="dot dot-middle-top"></span>
				<span class="dot dot-middle-bottom"></span>
				<span class="dot dot-bottom"></span>
			</div>
			<span class="circle circle-bottom"></span>
		</div>
		<div class="line line3">
			<span class="circle circle-top"></span>
			<div class="dotted">
				<span class="dot dot-top"></span>
				<span class="dot dot-middle-top"></span>
				<span class="dot dot-middle-bottom"></span>
				<span class="dot dot-bottom"></span>
			</div>
			<span class="circle circle-bottom"></span>
		</div>
		<div class="line line4">
			<span class="circle circle-top"></span>
			<div class="dotted">
				<span class="dot dot-top"></span>
				<span class="dot dot-middle-top"></span>
				<span class="dot dot-middle-bottom"></span>
				<span class="dot dot-bottom"></span>
			</div>
			<span class="circle circle-bottom"></span>
		</div>
		<div class="line line5">
			<span class="circle circle-top"></span>
			<div class="dotted">
				<span class="dot dot-top"></span>
				<span class="dot dot-middle-top"></span>
				<span class="dot dot-middle-bottom"></span>
				<span class="dot dot-bottom"></span>
			</div>
			<span class="circle circle-bottom"></span>
		</div>
		<div class="line line6">
			<span class="circle circle-top"></span>
			<div class="dotted">
				<span class="dot dot-top"></span>
				<span class="dot dot-middle-top"></span>
				<span class="dot dot-middle-bottom"></span>
				<span class="dot dot-bottom"></span>
			</div>
			<span class="circle circle-bottom"></span>
		</div>
		<div class="line line7">
			<span class="circle circle-top"></span>
			<div class="dotted">
				<span class="dot dot-top"></span>
				<span class="dot dot-middle-top"></span>
				<span class="dot dot-middle-bottom"></span>
				<span class="dot dot-bottom"></span>
			</div>
			<span class="circle circle-bottom"></span>
		</div>
		<div class="line line8">
			<span class="circle circle-top"></span>
			<div class="dotted">
				<span class="dot dot-top"></span>
				<span class="dot dot-middle-top"></span>
				<span class="dot dot-middle-bottom"></span>
				<span class="dot dot-bottom"></span>
			</div>
			<span class="circle circle-bottom"></span>
		</div>
		<div class="line line9">
			<span class="circle circle-top"></span>
			<div class="dotted">
				<span class="dot dot-top"></span>
				<span class="dot dot-middle-top"></span>
				<span class="dot dot-middle-bottom"></span>
				<span class="dot dot-bottom"></span>
			</div>
			<span class="circle circle-bottom"></span>
		</div>
		<div class="line line10">
			<span class="circle circle-top"></span>
			<div class="dotted">
				<span class="dot dot-top"></span>
				<span class="dot dot-middle-top"></span>
				<span class="dot dot-middle-bottom"></span>
				<span class="dot dot-bottom"></span>
			</div>
			<span class="circle circle-bottom"></span>
		</div>
		<div class="line line11">
			<span class="circle circle-top"></span>
			<div class="dotted">
				<span class="dot dot-top"></span>
				<span class="dot dot-middle-top"></span>
				<span class="dot dot-middle-bottom"></span>
				<span class="dot dot-bottom"></span>
			</div>
			<span class="circle circle-bottom"></span>
		</div>
	</div>
</body>
<%--<body>
    <form id="form1" runat="server">
        <div id="heading">
            <h1>hello this is a website goodbye</h1>
        </div>

        <div id="source">

        </div>
    </form>
</body>--%>
</html>
